# Mushroom Invaders

![alt text](Main.jpg)

True ninja that exists even if defeated by the mushrooms.

Play to know more.....

## Installation and Setup:

1. Download the [Mushi.zip](Mushi.zip)
2. Extract its contents into a folder
3. Navigate to the folder and launch the Mushi.exe 


## Controls:

WASD - Movement Keys

Space Key - Shoot

## Features:
* Toggle enemies using: 1) Default Mushrooms, 2) White Ship, 3) Oversized Mushrooms
* Toggle Sound using: 4) Bullet , 5) Laser


## Working:

1) Can interact with enemies - toggleable between 'Mushrooms' and 'enemy(white ship)'

2) Collision

3) Bounded boxes

## To-do:

1) <s>Sound Effects</s> - Working, toggleable between 'alt' and 'laser'

2) <s>Resize Mushrooms</s>

3) <s>Player Vertical Movement</s> - Working

4) <s>Score Display</s> - No. of Enemies Defeated



## Assets:

SpaceShip and Bullets : https://opengameart.org/content/aircrafts

Oversized-Mushroom / Ninja Man: https://www.flaticon.com/packs/game-assets

Sound: https://freesound.org/people/LittleRobotSoundFactory/packs/16689/

